#pragma once
#include <string>
#include <vector>
#include <stdexcept>
#include <jsoncons/json.hpp>
#include "piece.h"
#include <jsoncons/json.hpp>

const std::string ID = "id";
const std::string NAME = "name";
const std::string PIECES = "pieces";
const std::string LANES = "lanes";

class track
{
public:
	track() = default;
	explicit track(const jsoncons::json& msg)
	{
		m_id = msg.get(ID).as<std::string>(); 
		m_name = msg.get(NAME).as<std::string>();

		// track pieces
		const auto & pieces = msg.get(PIECES);
		for(size_t n = 0; n < pieces.size(); n++)
		{
			m_pieces.push_back( piece(pieces[n]) );
		}

		const auto & lanes = msg.get(LANES);
		m_lane_dists.resize(lanes.size());
		for(size_t n = 0; n < lanes.size(); n++)
		{
			int index = lanes[n].get("index").as< int >();
			m_lane_dists[index] = lanes[n].get("distanceFromCenter").as< double >();
		}
	}

	const piece & get_piece(size_t index) const
	{
		return m_pieces[index % m_pieces.size()];
	}

	size_t num_pieces() const
	{
		return m_pieces.size();
	}

	double get_lane_dist(size_t index) const
	{
		if( index >= m_lane_dists.size() )
			throw std::out_of_range("Tried to acces lane distance of non-existing lane");
		return m_lane_dists[index]; // negative = to the left, positive = to the right
	}

	size_t num_lanes() const
	{
		return m_lane_dists.size();
	}

private:
	std::string m_id;
	std::string m_name;
	std::vector<piece> m_pieces;
	std::vector<double> m_lane_dists;
};

