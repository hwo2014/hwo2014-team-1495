#include "game_logic.h"
#include "protocol.h"
#include "utils.h"
#include <fstream>
#include <cstdlib>

using namespace hwo_protocol;

#define SAVE_DATA false

//TODO add defensive coding
game_logic::game_logic()
  : action_map
{
	{ "join", &game_logic::on_join },
	{ "gameStart", &game_logic::on_game_start },
	{ "carPositions", &game_logic::on_car_positions },
	{ "crash", &game_logic::on_crash },
	{ "gameEnd", &game_logic::on_game_end },
	{ "error", &game_logic::on_error },
	{ "yourCar", &game_logic::on_yourCar },
	{ "gameInit", &game_logic::on_gameInit},
	{ "lapFinished", &game_logic::on_lapFinished },
	{ "finish", &game_logic::on_finish },
	{ "tournamentEnd", &game_logic::on_end },
	{ "spawn", &game_logic::on_spawn },
	{ "dnf", &game_logic::on_dnf }
	
}
{
}

game_logic::msg_vector game_logic::react(const jsoncons::json& msg)
{
	const auto& msg_type = msg.get("msgType","").as<std::string>();
	const auto& data = msg.get("data");
	auto action_it = action_map.find(msg_type);
	// TODO check correct game ID?
	//To see communication from server side
	if (SAVE_DATA) // TODO is this OK when run on the server?
	{
		std::ofstream commfile;
		commfile.open ("communication.txt",std::ios::app);
		commfile << msg;
		commfile << std::endl << std::endl;
		commfile.close();
	}
	if (action_it != action_map.end())
	{
		/*if( msg_type == "carPositions" ) // print game tick
			std::cout << "Game tick: " << msg.get("gameTick") << std::endl; */
		return (action_it->second)(this, data);
	}
	else
	{
		std::cout << "Unknown message type: " << msg_type << std::endl;
		return { make_ping() };
	}
}

game_logic::msg_vector game_logic::on_join(const jsoncons::json& data)
{
	std::cout << "Joined" << std::endl;
	return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_start(const jsoncons::json& data)
{
	std::cout << "Race started" << std::endl;
	return { make_ping() };
}

game_logic::msg_vector game_logic::on_car_positions(const jsoncons::json& data)
{
	ai.ai_main(data);
	if (ai.shouldSwitchLane())
	{
		ai.sent_lane_switch();
		return {make_request("switchLane", ai.whichWay())};
	}
	return { make_throttle(ai.whichThrottle()) };
}

game_logic::msg_vector game_logic::on_crash(const jsoncons::json& data)
{
	std::cout << "Someone crashed!" << std::endl;
	return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_end(const jsoncons::json& data)
{
	std::cout << "Race ended!" << std::endl;
	return { make_ping() };
}

game_logic::msg_vector game_logic::on_error(const jsoncons::json& data)
{
	std::cout << "Error: " << data.to_string() << std::endl;
	return { make_ping() };
}

game_logic::msg_vector game_logic::on_yourCar(const jsoncons::json& data)
{
	std::cout << "Got car info!" << std::endl;
	car_id id(data);
	ai.set_car_id(id);
	return { make_ping() };
}
game_logic::msg_vector game_logic::on_gameInit(const jsoncons::json& data)
{
	std::cout << "Gentlemen start your calculations!" << std::endl;

	// save track data
	const auto & r = data.get("race");
	const auto & t = r.get("track");
	ai.setTrack(track(t));
	return { make_ping() };
}
game_logic::msg_vector game_logic::on_lapFinished(const jsoncons::json& data)
{
	std::cout << "Hurray the car finished a lap!" << std::endl;
	return { make_ping() };
}

game_logic::msg_vector game_logic::on_finish(const jsoncons::json& data)
{
	std::cout << "Woho the car finished the race!" << std::endl;
	return { make_ping() };
}

game_logic::msg_vector game_logic::on_end(const jsoncons::json& data)
{
	std::cout << "Tournament ended, hope we won!" << std::endl;
	return { make_ping() };
}

game_logic::msg_vector game_logic::on_spawn(const jsoncons::json& data)
{
	std::cout << "Someone spawned!" << std::endl;
	return { make_ping() };
}

game_logic::msg_vector game_logic::on_dnf(const jsoncons::json& data)
{
	std::cout << "Ohh fuck we got disqualified!" << std::endl;
	return { make_ping() };
}




