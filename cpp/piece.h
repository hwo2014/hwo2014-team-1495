#pragma once
#include <string>
#include <jsoncons/json.hpp>

const std::string SWITCH = "switch";
const std::string RADIUS = "radius";
const std::string LENGTH = "length";
const std::string ANGLE = "angle";

constexpr double PI = acos(0)*2;

class piece
{
public:
	piece() = default;
	piece(const jsoncons::json& msg)
	{
		m_switch = msg.get(SWITCH, false).as< decltype(m_switch) >();
		m_radius = msg.get(RADIUS, RADIUS_UNDEF).as< decltype(m_radius) >();
		m_angle = msg.get(ANGLE, ANGLE_DEFAULT).as< decltype(m_angle) >();
		m_curved = !(m_radius == RADIUS_UNDEF);
		if( m_curved )
			m_length = fabs(m_angle) / 180.0 * PI * m_radius;
		else
			m_length = msg.get(LENGTH, LENGTH_UNDEF).as< decltype(m_length) >();
	}

	constexpr static double RADIUS_UNDEF = -1.0;
	constexpr static int LENGTH_UNDEF = -1;
	constexpr static double ANGLE_DEFAULT = 0.0;

	bool has_switch() const
	{
		return m_switch;
	}

	bool is_curved() const
	{
		return m_curved;
	}

	int length() const
	{
		return m_length; // might be undefined!
	}

	double radius() const
	{
		return m_radius; // might be undefined!
	}

	// negative = left turn, positive = right turn
	double angle() const // in degrees!
	{
		return m_angle;
	}

private:
	bool m_switch;
	bool m_curved;
	int m_length;
	double m_radius;
	double m_angle;
};
