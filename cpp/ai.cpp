#include "ai.h"
#include "utils.h"
#include <iostream>
#include <string>
using namespace std;

const string LEFT = "Left";
const string RIGHT = "Right";

void our_ai::ai_main(const jsoncons::json& data)
{
	for(size_t n = 0; n < data.size(); n++)
	{
		car new_car(data[n]); // TODO update all cars
		if( new_car.get_id() == m_cur_car.get_id() )
		{
			update_car(new_car);
			break;
		}
	}

	if( m_switch_situation == SWITCH_ACTION::WAITING_FOR_SWITCH && passed_switch(m_cur_car) )
		m_switch_situation = SWITCH_ACTION::NO_SWITCH;

	m_throttle = 0.5;
	int next_piece = m_cur_car.get_pos().piece_index + 1;
	if( m_switch_situation == SWITCH_ACTION::NO_SWITCH && m_track.get_piece(next_piece).has_switch() )
	{

		int cur_lane_index = m_cur_car.get_pos().lane_pos.start_index;
		/*int other_lane_index = 1 - cur_lane_index;
		bool switch_right = (m_track.get_lane_dist(cur_lane_index) < 
				m_track.get_lane_dist(other_lane_index)); // currently to the left of other lane*/
		Direction d = shortest_lane(m_track, m_cur_car.get_pos());
		if( d == Direction::LEFT || d == Direction::RIGHT )
		{
			m_direction = dir_to_str(d);
			m_switch_situation = SWITCH_ACTION::NEED_SWITCH;
		}
		// else if d == Direction::FORWARD, do nothing
		//m_direction = switch_right ? RIGHT : LEFT;
	}
	cout << car_speed() << " " << m_cur_car.get_pos().in_piece_dist << endl; // print our car speed
}


void our_ai::setTrack(const track & t)
{
	m_track = t;
}

void our_ai::update_car(const car & c)
{
	if( m_cur_car.get_id() == c.get_id() )
	{
		m_prev_car = m_cur_car;
		m_cur_car = c;
		if( !m_has_updated_car )
			m_prev_car = m_cur_car; // set prev to start position if this is the first info we get
		m_has_updated_car = true;
	}
}

void our_ai::set_car_id(const car_id & id)
{
	m_cur_car.set_id(id);
}

bool our_ai::shouldSwitchLane()
{
	return (m_switch_situation == SWITCH_ACTION::NEED_SWITCH);
}

void our_ai::sent_lane_switch()
{
	m_switch_situation = SWITCH_ACTION::WAITING_FOR_SWITCH;
}

std::string our_ai::whichWay()
{
	return m_direction;
}

double our_ai::whichThrottle()
{
	return m_throttle;
}

double our_ai::car_speed() const
{
	if( m_has_updated_car ) 
		return distance(m_prev_car.get_pos(), m_cur_car.get_pos(), m_track);
	else
		return 0.0;
}

bool our_ai::passed_switch(const car & c)
{
	int p_index = c.get_pos().piece_index;
	const auto & cur_piece = m_track.get_piece(p_index);
	if( cur_piece.has_switch() )
	{
		if( c.get_pos().in_piece_dist > cur_piece.length()/2 ) // TODO length not correct depending on lane in curve but should be decent...
			return true;
	}
	return false;
}

