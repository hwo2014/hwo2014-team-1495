#include <iostream>
#include <string>
#include <jsoncons/json.hpp>
#include <cstdlib>
#include "protocol.h"
#include "connection.h"
#include "game_logic.h"

using namespace hwo_protocol;

void run(hwo_connection& connection, const std::string& name, const std::string& key)
{
	game_logic game;
	for (;;)
	{
		boost::system::error_code error;
		auto response = connection.receive_response(error);

		if (error == boost::asio::error::eof)
		{
			std::cout << "Connection closed" << std::endl;
			break;
		}
		else if (error)
		{
			throw boost::system::system_error(error);
		}

		connection.send_requests(game.react(response));
	}
}

int main(int argc, const char* argv[])
{
	try
	{
		if (argc < 5)
		{
			std::cerr << "Usage: ./run host port botname botkey (trackname) (password) (nr cars) (host/join)" << std::endl;
			return 1;
		}
		const std::string host(argv[1]);
		const std::string port(argv[2]);
		const std::string name(argv[3]);
		const std::string key(argv[4]);
		hwo_connection connection(host, port);
		if (argc == 5)
		{
			std::cout << "Host: " << host << ", port: " << port << ", name: " << name << ", key:" << key << std::endl;
			connection.send_requests({ make_join(name, key) });	
		}
		else if (argc < 9)
		{
			std::cerr << "Usage: ./run host port botname botkey (trackname) (password) (nr cars) (host/join)" << std::endl;
			return 1;
		}
		else if (argc == 9)
		{
			const std::string trackName(argv[5]);
			const std::string password(argv[6]);
			int nr_cars = atoi(argv[7]);
			const std::string toHost(argv[8]);
			if (toHost == "host")
			{
				std::cout << "Host: " << host << ", port: " << port << ", name: " << name << ", key:" << key << std::endl;
				std::cout << "Created: " << trackName << ", password: " << password << ", cars: " << nr_cars << std::endl;
				connection.send_requests({make_create(name,key,trackName,password,nr_cars)});
			}
			else
			{
				std::cout << "Host: " << host << ", port: " << port << ", name: " << name << ", key:" << key << std::endl;
				std::cout << "Joined: " << trackName << ", password: " << password << ", cars: " << nr_cars << std::endl;
				connection.send_requests({make_joinCreated(name,key,trackName,password,nr_cars)});
			}
				
		}
		else
		{
			std::cerr << "Usage: ./run host port botname botkey (trackname) (password) (nr cars) (host/join)" << std::endl;
			return 1;
		}
		
		
		run(connection, name, key);
	}
	catch (const std::exception& e)
	{
		std::cerr << e.what() << std::endl;
		return 2;
	}

	return 0;
}
