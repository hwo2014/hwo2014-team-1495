#pragma once
#include <string>
#include <jsoncons/json.hpp>

// TODO move to own file?
struct lane
{
	lane() = default;
	explicit lane(const jsoncons::json & msg)
	{
		start_index = msg.get("startLaneIndex").as< int >();
		end_index = msg.get("endLaneIndex").as< int >();
	}

	int start_index;
	int end_index;
};

// TODO move to own file?
struct piecePos
{
	piecePos() = default;
	explicit piecePos(const jsoncons::json & msg)
	{
		piece_index = msg.get("pieceIndex").as< decltype(piece_index) >();
		in_piece_dist = msg.get("inPieceDistance").as< decltype(in_piece_dist) >();
		lane_pos = lane(msg.get("lane"));
		lap = msg.get("lap").as< int >();
	}

	int piece_index;
	double in_piece_dist;
	lane lane_pos;
	int lap;
};

struct car_id
{
	car_id() = default;
	explicit car_id(const jsoncons::json & msg)
	{
		name = msg.get("name").as< std::string >();
		color = msg.get("color").as< std::string >();
	}

	bool operator== (const car_id & other) const
	{
		return name == other.name &&
			color == other.color;
	}

	std::string name, color;
};

// TODO dimensions, also car info is sent at different times
// -- game init -> car ids/dimensions
// -- ticks -> car ids/positions
class car
{
public:
	car() = default;
	explicit car(const jsoncons::json & msg)
	{
		m_id = car_id(msg.get("id"));
		m_angle = msg.get("angle").as< double >();
		m_pos = piecePos( msg.get("piecePosition") );
	}

	const piecePos & get_pos() const
	{
		return m_pos;
	}

	double angle() const // in degrees!
	{
		return m_angle;
	}

	void set_id(const car_id & id)
	{
		m_id = id;
	}
	
	const car_id & get_id() const
	{
		return m_id;
	}

private:
	car_id m_id;

	double m_angle;
	piecePos m_pos;
};

