#include "utils.h"
#include <algorithm>

#include <iostream> // TODO remove
using namespace std;

/*
	-1 if d < 0
	0 if d == 0
	1 if d > 0
*/
double sgn(double d)
{
	return (d > 0) - (d < 0);
}

/*
	length of piece p on lane with lane_dist units from center
 */
double curved_piece_len(const piece & p, const double lane_dist) // magic
{
	return fabs(p.angle()) / 180.0 * PI * 
		(p.radius() + fabs(lane_dist) * -sgn(p.angle()*lane_dist));
}

double lane_piece_length(const track & tr, const int piece_index, const int lane)
{
	if( tr.get_piece(piece_index).is_curved() )
		return curved_piece_len(tr.get_piece(piece_index), tr.get_lane_dist(lane));
	else
		return tr.get_piece(piece_index).length();
}

// function calculating length of piece given start_lane and end_lane
// TODO correct?
double driving_length(const track & tr, const int piece_index, const int start_lane, const int end_lane)
{
	double lane_diff = tr.get_lane_dist(end_lane) - tr.get_lane_dist(start_lane);

	double short_lane_length = min( 
			lane_piece_length(tr, piece_index, start_lane), 
			lane_piece_length(tr, piece_index, end_lane));

	return sqrt(lane_diff * lane_diff + short_lane_length * short_lane_length); // calculate hypothenuse between lanes
	// TODO still too short sometimes
}

// TODO use the function driving_length
double distance(const piecePos & lhs, const piecePos & rhs, const track & t)
{
	if( lhs.piece_index == rhs.piece_index )
		return abs(lhs.in_piece_dist - rhs.in_piece_dist);
	else
	{
		double tl = 0; // track length
		for(int n = 0; n < t.num_pieces(); n++)
			tl += t.get_piece(n).length();

		// dist to end of lhs piece
		const auto & lhs_piece = t.get_piece(lhs.piece_index);
		double d = 0;
		// distance until end of lhs piece
		d += driving_length(t, lhs.piece_index, lhs.lane_pos.start_index, lhs.lane_pos.end_index) - lhs.in_piece_dist; 

		// sum of distances driven in pieces between lhs and rhs
		for(int n = (lhs.piece_index+1) % t.num_pieces(); n != rhs.piece_index; n = (n+1) % t.num_pieces())
			d += lane_piece_length(t, n, lhs.lane_pos.end_index);

		// distance into rhs piece
		d += rhs.in_piece_dist; 

		d = min(d, tl - d); // TODO distance between cars should depend on the lap counters
		return d;
	}
}


string dir_to_str(const Direction & dir)
{
	switch(dir)
	{
		case Direction::LEFT:
			return "Left";
		case Direction::RIGHT:
			return "Right";
		case Direction::FORWARD:
			return "";
	}
	return "";
}

// returns the index of the closest piece (counting forward)
// that contains a switch
int next_switch_piece(const track & tr, int piece_index)
{
	int ret = piece_index;
	while( !tr.get_piece(ret).has_switch() )
		ret = (ret+1) % tr.num_pieces();
	return ret % tr.num_pieces();
}

// returns direction to turn to get from start_lane to end_lane
Direction turn_dir(const track & tr, const int start_lane, const int end_lane)
{
	double start_dist = tr.get_lane_dist(start_lane),
			 end_dist = tr.get_lane_dist(end_lane);
	if( start_dist > end_dist )
		return Direction::LEFT;
	else if( start_dist < end_dist )
		return Direction::RIGHT;
	else
		return Direction::FORWARD;
}

/*
	calculate which lane to take (in next switch) to drive shortest
	path until switch after that
	*/
// TODO this function only finds the best lane between the next switch-piece
// and the switch-piece after that.
// Thus it might not give the best lane when it comes to finishing a lap/race
// TODO does not count distance increase of switching lane
Direction shortest_lane(const track & tr, const piecePos & pos)
{
	int start_lane = pos.lane_pos.end_index; // we cannot switch in the current piece (so we look at the end lane of the current piece)

	int start_piece = next_switch_piece(tr, pos.piece_index+1);
	int end_piece = next_switch_piece(tr, start_piece+1);

	int best_dist = numeric_limits<int>::max();
	int best_lane = start_lane;
	for(int lane = start_lane-1; lane <= start_lane+1; lane++) // look at lane to the left and to the right (and the current one)
	{
		if( lane >= 0 && lane < tr.num_lanes() ) // lane must exist
		{
			int cur_dist = driving_length(tr, start_piece, start_lane, lane); // switching lane from start_lane to lane at start_piece
			for(int p = (start_piece+1) % tr.num_pieces(); p != end_piece; p = (p+1) % tr.num_pieces())
			{
				cur_dist += driving_length(tr, p, lane, lane); // driving on lane from end of start_piece to beginning of end_piece
			}
			if( best_dist > cur_dist )
			{
				best_dist = cur_dist;
				best_lane = lane;
			}
		}
	}
	return turn_dir(tr, start_lane, best_lane);
}

