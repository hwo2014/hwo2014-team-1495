#pragma once
#include "car.h"
#include "track.h"
#include <string>

enum class Direction 
{
	LEFT,
	RIGHT,
	FORWARD
};

// Direction to string
std::string dir_to_str(const Direction & dir);

// distance between lhs and rhs
double distance(const piecePos & lhs, const piecePos & rhs, const track & t);

// shortest lane between next two switches after pos
Direction shortest_lane(const track & tr, const piecePos & pos);
