#ifndef AI_H
#define AI_H

#include <string>
#include <iostream>
#include "car.h"
#include "track.h"
#include <jsoncons/json.hpp>

enum class SWITCH_ACTION
{
	NO_SWITCH,
	NEED_SWITCH,
	WAITING_FOR_SWITCH
};

class our_ai
{
public: 
	void ai_main(const jsoncons::json& data);
	void setTrack(const track & t);
	void update_car(const car & c);
	void set_car_id(const car_id & id);
	bool shouldSwitchLane();
	void sent_lane_switch();
	std::string whichWay();
	double whichThrottle();
	double car_speed() const;
	bool passed_switch(const car & c);

private:
	car m_prev_car, m_cur_car; // m_cur_car is not initilized decently unless m_has_updated_car is set
	track m_track;
	double m_throttle, m_speed;
	bool m_has_updated_car = false;
	SWITCH_ACTION m_switch_situation = SWITCH_ACTION::NO_SWITCH;
	std::string m_direction;
};

#endif
